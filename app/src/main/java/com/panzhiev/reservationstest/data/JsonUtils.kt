package com.panzhiev.reservationstest.data

import android.content.Context
import java.io.IOException
import java.nio.charset.Charset

/**
 * Utility method for reading json file from assets folder.
 * @param context - any type of Context
 * @param fileName - name of json file.
 * @return nullable json string.
 * */
fun getJsonFromAssets(context: Context, fileName: String) = try {
    val inputStream = context.assets.open(fileName)
    val size: Int = inputStream.available()
    val buffer = ByteArray(size)
    inputStream.apply {
        read(buffer)
        close()
    }
    String(buffer, Charset.forName("UTF-8"))
} catch (e: IOException) {
    e.printStackTrace()
    null
}