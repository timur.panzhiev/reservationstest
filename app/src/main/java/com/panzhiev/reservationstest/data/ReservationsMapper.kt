package com.panzhiev.reservationstest.data

import com.google.gson.Gson
import com.panzhiev.reservationstest.data.model.*
import org.json.JSONArray
import java.lang.Exception
import java.lang.UnsupportedOperationException

/**
 * Extension function for mapping Json with Reservations array to list of [Reservation]
 *
 * @return list of reservations.
 * */
fun String.toReservations(): List<Reservation> =
    try {
        val jsonArray = JSONArray(this)
        val reservations = arrayListOf<Reservation>()

        val gson = Gson()

        for (i in 0 until jsonArray.length()) {
            val jsonObj = jsonArray.getJSONObject(i)
            val reservation =
                try {
                    gson.fromJson(
                        jsonObj.toString(),
                        when (val type = jsonObj.getString("type")) {
                            ReservationType.E_BIKE -> EBikeReservation::class.java
                            ReservationType.SHUTTLE -> ShuttleReservation::class.java
                            ReservationType.PUBLIC_TRANSPORT -> PublicTransportReservation::class.java
                            else -> throw UnsupportedOperationException("Unknown reservation type: $type")
                        }
                    )
                } catch (e: Exception) {
                    e.printStackTrace()
                    null
                }

            if (reservation != null) {
                reservations.add(reservation)
            }
        }

        reservations
    } catch (e: Exception) {
        e.printStackTrace()
        listOf()
    }