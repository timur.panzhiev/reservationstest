package com.panzhiev.reservationstest.data

import com.panzhiev.reservationstest.data.model.Reservation

/**
 * Repository class for working with Reservations data. Implemented in [ReservationsRepositoryImpl].
 * */
interface ReservationsRepository {

    /**
     * Function for fetching list of [Reservation]
     * from server (or in our case from local json file).
     *
     * @return list of all reservations.
     * */
    fun getReservations(): List<Reservation>
}