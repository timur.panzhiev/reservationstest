package com.panzhiev.reservationstest.data

import android.content.Context
import com.panzhiev.reservationstest.data.model.Reservation

private const val RESERVATIONS_JSON_FILE_NAME = "reservations.json"

class ReservationsRepositoryImpl(
    private val context: Context
) : ReservationsRepository {

    override fun getReservations(): List<Reservation> =
        getJsonFromAssets(context, RESERVATIONS_JSON_FILE_NAME)?.toReservations() ?: listOf()
}