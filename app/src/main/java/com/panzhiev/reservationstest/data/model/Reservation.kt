package com.panzhiev.reservationstest.data.model

import com.google.gson.annotations.SerializedName

/**
 * Base class for any type of Reservation. Currently there are three type of Reservation:
 * [EBikeReservation], [ShuttleReservation], [PublicTransportReservation]
 * */
abstract class Reservation {
    abstract val type: String
    abstract val vehicle: Vehicle
    abstract val from: String
    abstract val to: String
}

data class EBikeReservation(
    override val type: String,
    override val vehicle: EBikeVehicle,
    override val from: String,
    override val to: String
) : Reservation()

data class ShuttleReservation(
    override val type: String,
    override val vehicle: ShuttleVehicle,
    @SerializedName("line_color") val lineColor: String,
    @SerializedName("line_identifier") val lineIdentifier: String,
    override val from: String,
    override val to: String
) : Reservation()

data class PublicTransportReservation(
    override val type: String,
    override val vehicle: PublicTransportVehicle,
    @SerializedName("travel_class") val travelClass: String,
    @SerializedName("line_identifier") val lineIdentifier: String,
    override val from: String,
    override val to: String
) : Reservation()