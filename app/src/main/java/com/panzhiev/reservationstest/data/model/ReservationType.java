package com.panzhiev.reservationstest.data.model;

import androidx.annotation.StringDef;

/**
 * An interface that contains all possible types of reservation. This element is also used
 * for filtering Reservations on Reservations Screen.
 */

@StringDef({
        ReservationType.E_BIKE,
        ReservationType.SHUTTLE,
        ReservationType.PUBLIC_TRANSPORT,
        ReservationType.ALL,
})
public @interface ReservationType {
    String E_BIKE = "e_bike";
    String SHUTTLE = "shuttle";
    String PUBLIC_TRANSPORT = "public_transport";
    String ALL = "all";
}