package com.panzhiev.reservationstest.data.model

import com.google.gson.annotations.SerializedName

/**
 * Base class for any type of Vehicle. Currently there are three type of Vehicle:
 * [EBikeVehicle], [ShuttleVehicle], [PublicTransportVehicle]. Every vehicle has its own information
 * depends on [ReservationType].
 * */
abstract class Vehicle {
    abstract val type: String
}

data class EBikeVehicle(
    override val type: String,
    val tag: Int,
    val size: String
) : Vehicle()

data class ShuttleVehicle(
    override val type: String,
    @SerializedName("available_seats") val availableSeats: Int,
    @SerializedName("wheelchair_loading") val wheelchairLoading: Boolean
) : Vehicle()

data class PublicTransportVehicle(
    override val type: String,
    @SerializedName("train_type") val trainType: String,
    val restaurant: Boolean
) : Vehicle()