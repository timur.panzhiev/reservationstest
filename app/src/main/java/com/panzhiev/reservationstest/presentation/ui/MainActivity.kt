package com.panzhiev.reservationstest.presentation.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.panzhiev.reservationstest.R
import com.panzhiev.reservationstest.data.ReservationsRepositoryImpl
import com.panzhiev.reservationstest.data.model.ReservationType
import com.panzhiev.reservationstest.databinding.ActivityMainBinding
import com.panzhiev.reservationstest.presentation.viewmodel.ReservationsViewModel

/**
 * Main application screen that represents list of Reservations
 * and provides filtering of Reservations.
 * */
class MainActivity : AppCompatActivity(), ReservationTypeCallback {

    private lateinit var binding: ActivityMainBinding

    private val viewModelFactory by lazy {
        ReservationsViewModelFactory(
            ReservationsRepositoryImpl(applicationContext)
        )
    }

    private val viewModel by lazy {
        ViewModelProvider(this, viewModelFactory).get(ReservationsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        setupViews()
    }

    private fun setupViews() {
        binding.apply {
            vm = viewModel
            reservationsRv.apply {
                adapter = ReservationsAdapter()
                addItemDecoration(SpacingDecoration(context))
            }
            filterBtn.setOnClickListener {
                ReservationTypeDialogFragment
                    .newInstance()
                    .show(supportFragmentManager, ReservationTypeDialogFragment.TAG)
            }
        }
    }

    override fun onTypeSelected(@ReservationType type: String) = viewModel.setType(type)
}