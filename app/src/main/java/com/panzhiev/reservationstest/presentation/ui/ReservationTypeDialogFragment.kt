package com.panzhiev.reservationstest.presentation.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.panzhiev.reservationstest.R
import com.panzhiev.reservationstest.data.model.ReservationType
import com.panzhiev.reservationstest.databinding.DialogFragmentReservationTypeBinding

/**
 * Screen that represents list of [ReservationType]
 * and allows user select specific [ReservationType] to filter Reservations.
 *
 * */
class ReservationTypeDialogFragment : BottomSheetDialogFragment() {

    private lateinit var binding: DialogFragmentReservationTypeBinding
    private lateinit var reservationTypeCallback: ReservationTypeCallback

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.dialog_fragment_reservation_type,
            container,
            false
        )
        binding.lifecycleOwner = this
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupViews()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.reservationTypeCallback = context as ReservationTypeCallback
    }

    private fun setupViews() {
        binding.apply {
            eBikeTv.setOnClickListener {
                reservationTypeCallback.onTypeSelected(ReservationType.E_BIKE)
                dismiss()
            }
            shuttleTv.setOnClickListener {
                reservationTypeCallback.onTypeSelected(ReservationType.SHUTTLE)
                dismiss()
            }
            publicTransportTv.setOnClickListener {
                reservationTypeCallback.onTypeSelected(ReservationType.PUBLIC_TRANSPORT)
                dismiss()
            }
            allTv.setOnClickListener {
                reservationTypeCallback.onTypeSelected(ReservationType.ALL)
                dismiss()
            }
        }
    }

    companion object {
        val TAG = ReservationTypeDialogFragment::class.java.name
        fun newInstance() = ReservationTypeDialogFragment()
    }
}

interface ReservationTypeCallback {
    fun onTypeSelected(@ReservationType type: String)
}