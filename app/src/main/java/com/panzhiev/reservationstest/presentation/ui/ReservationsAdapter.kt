package com.panzhiev.reservationstest.presentation.ui

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.panzhiev.reservationstest.R
import com.panzhiev.reservationstest.data.model.*
import com.panzhiev.reservationstest.databinding.ItemReservationEBikeBinding
import com.panzhiev.reservationstest.databinding.ItemReservationPublicTransportBinding
import com.panzhiev.reservationstest.databinding.ItemReservationShuttleBinding

/**
 * Common Adapter for any type of Reservation.
 * Contains three view type depending on type of Reservation. [ReservationType]
 * */
class ReservationsAdapter : ListAdapter<Reservation, RecyclerView.ViewHolder>(DiffCallback) {

    override fun getItemViewType(position: Int): Int =
        when (getItem(position).type) {
            ReservationType.E_BIKE -> ViewType.E_BIKE
            ReservationType.SHUTTLE -> ViewType.SHUTTLE
            else -> ViewType.PUBLIC_TRANSPORT
        }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder =
        when (viewType) {
            ViewType.E_BIKE -> EBikeViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_reservation_e_bike,
                    parent,
                    false
                )
            )
            ViewType.SHUTTLE -> ShuttleViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_reservation_shuttle,
                    parent,
                    false
                )
            )
            else -> PublicTransportViewHolder(
                DataBindingUtil.inflate(
                    LayoutInflater.from(parent.context),
                    R.layout.item_reservation_public_transport,
                    parent,
                    false
                )
            )
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder.itemViewType) {
            ViewType.E_BIKE -> (holder as EBikeViewHolder).bind(getItem(position) as EBikeReservation)
            ViewType.SHUTTLE -> (holder as ShuttleViewHolder).bind(getItem(position) as ShuttleReservation)
            else -> (holder as PublicTransportViewHolder).bind(getItem(position) as PublicTransportReservation)
        }
    }

    inner class EBikeViewHolder(private val binding: ItemReservationEBikeBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: EBikeReservation) {
            binding.apply {
                reservationTypeValue.text = item.type
                vehicleTypeValue.text = item.vehicle.type
                vehicleTagValue.text = item.vehicle.tag.toString()
                vehicleSizeValue.text = item.vehicle.size
                reservationFromValue.text = item.from
                reservationToValue.text = item.to
            }
        }
    }

    inner class ShuttleViewHolder(private val binding: ItemReservationShuttleBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ShuttleReservation) {
            binding.apply {
                reservationTypeValue.text = item.type
                vehicleTypeValue.text = item.vehicle.type
                availableSeatsValue.text = item.vehicle.availableSeats.toString()
                wheelchairLoadingValue.text = item.vehicle.wheelchairLoading.toReadableString()
                lineColorValue.setBackgroundColor(Color.parseColor(item.lineColor))
                lineIdentifierValue.text = item.lineIdentifier
                reservationFromValue.text = item.from
                reservationToValue.text = item.to
            }
        }
    }

    inner class PublicTransportViewHolder(private val binding: ItemReservationPublicTransportBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: PublicTransportReservation) {
            binding.apply {
                reservationTypeValue.text = item.type
                vehicleTypeValue.text = item.vehicle.type
                trainTypeValue.text = item.vehicle.trainType
                restaurantValue.text = item.vehicle.restaurant.toReadableString()
                travelClassValue.text = item.travelClass
                lineIdentifierValue.text = item.lineIdentifier
                reservationFromValue.text = item.from
                reservationToValue.text = item.to
            }
        }
    }

    object DiffCallback : DiffUtil.ItemCallback<Reservation>() {
        override fun areItemsTheSame(oldItem: Reservation, newItem: Reservation) = false
        override fun areContentsTheSame(oldItem: Reservation, newItem: Reservation) = false
    }

    private object ViewType {
        const val E_BIKE = 0
        const val SHUTTLE = 1
        const val PUBLIC_TRANSPORT = 2
    }
}

/**
 * Internal utility function for representation Boolean variable as human readable String.
 * */
private fun Boolean.toReadableString() = if (this) "Yes" else "No"