package com.panzhiev.reservationstest.presentation.ui

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.panzhiev.reservationstest.data.model.Reservation

/**
 * Helper class for ui data bindings. Contains methods annotated with [BindingAdapter]
 * to represent some kind of data on UI.
 * */
object ReservationsBindings {

    /**
     * Method that allows to set list of Reservations to UI.
     * @param reservations - filtered or unfiltered list of reservations.
     * */
    @BindingAdapter("reservations")
    @JvmStatic
    fun RecyclerView.setReservations(reservations: List<Reservation>?) {
        (adapter as ReservationsAdapter).submitList(reservations)
    }
}