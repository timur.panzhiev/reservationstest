package com.panzhiev.reservationstest.presentation.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.panzhiev.reservationstest.data.ReservationsRepository
import com.panzhiev.reservationstest.presentation.viewmodel.ReservationsViewModel

/**
 * Implementation of [ViewModelProvider.Factory] allows to create [ReservationsViewModel] instance
 * with incoming constructor parameters.
 */
class ReservationsViewModelFactory(private val repository: ReservationsRepository) :
    ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ReservationsViewModel::class.java)) {
            return ReservationsViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}