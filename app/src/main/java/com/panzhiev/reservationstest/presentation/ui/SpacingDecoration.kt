package com.panzhiev.reservationstest.presentation.ui

import android.content.Context
import android.graphics.Rect
import android.util.DisplayMetrics
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

private const val DEFAULT_SPACING = 16f

/**
 * Class that allows to make space dividers between cells of [RecyclerView]
 * */
class SpacingDecoration(context: Context, spacing: Float = DEFAULT_SPACING) :
    RecyclerView.ItemDecoration() {

    private val space = getPixelsFromDp(context, spacing)

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val lm = parent.layoutManager
        if (lm is LinearLayoutManager) {
            val childPosition = parent.getChildAdapterPosition(view)
            val lastPosition = parent.adapter!!.itemCount - 1
            if (lm.orientation == LinearLayoutManager.HORIZONTAL) {
                outRect.left = space
                if (childPosition == lastPosition) {
                    outRect.right = space
                }
            } else {
                outRect.top = space
                if (childPosition == lastPosition) {
                    outRect.bottom = space
                }
            }
        }
    }

    private fun getPixelsFromDp(context: Context, dp: Float) =
        (dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)).toInt()
}
