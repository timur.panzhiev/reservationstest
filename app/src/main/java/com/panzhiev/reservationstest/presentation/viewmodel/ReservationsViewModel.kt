package com.panzhiev.reservationstest.presentation.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.panzhiev.reservationstest.data.ReservationsRepository
import com.panzhiev.reservationstest.data.model.Reservation
import com.panzhiev.reservationstest.data.model.ReservationType

/**
 * ViewModel class allows to work with data that is provided for Main application screen.
 * */
class ReservationsViewModel(
    private val repository: ReservationsRepository
) : ViewModel() {

    private var reservations = listOf<Reservation>()
    private var selectedType = ReservationType.ALL

    val filteredReservations = MutableLiveData<List<Reservation>>()

    init {
        fetchReservations()
    }

    private fun fetchReservations() {
        reservations = repository.getReservations()
        filterReservations()
    }

    private fun filterReservations() {
        val filtered =
            if (selectedType == ReservationType.ALL) reservations
            else reservations.filter { it.type == selectedType }

        filteredReservations.postValue(filtered)
    }

    fun setType(@ReservationType type: String) {
        selectedType = type
        filterReservations()
    }
}